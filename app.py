import base64
import time

import cv2

import numpy as np
from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/movenet')
def movenet():
    return render_template('movenet.html')


@app.route('/mediapipe_full')
def mediapipe_full():
    return render_template('mediapipe_full.html')


@app.route('/mediapipe_pose')
def mediapipe_pose():
    return render_template('mediapipe_pose.html')


# Variables to calculate FPS
last_frame_time = time.time()
frame_count = 0
fps = 0

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1234)
